
using Ioasys.Desafio.NET.Dominio.Interfaces;
using Ioasys.Desafio.NET.Infrastructure;
using Ioasys.Desafio.NET.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container

builder.Services.AddControllers();
builder.Services.AddSingleton<DbContext>();
builder.Services.AddSingleton<IFilmeService, FilmeService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}

app.UseRouting();
app.MapControllers();
app.UseAuthorization();

app.Run();
