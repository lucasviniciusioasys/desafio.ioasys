﻿using Ioasys.Desafio.NET.Dominio.Interfaces;
using Ioasys.Desafio.NET.Dominio.Models;
using Ioasys.Desafio.NET.Infrastructure;
using Ioasys.Desafio.NET.ViewModels;

namespace Ioasys.Desafio.NET.Services
{
    public class FilmeService : IFilmeService
    {
        private readonly DbContext dbContext;

        public FilmeService(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<Filme> BuscarByIdAsync(int filmeID)
        {
            var filme = dbContext.Filmes.Find(x => x.Id.Equals(filmeID));
            return await Task.FromResult(filme).ConfigureAwait(false);
        }

        public async Task<List<Filme>> BuscarTodosFilmes(CancellationToken cancellationToken)
        {
            var filmes = dbContext.Filmes.ToList();

            return await Task.FromResult(filmes).ConfigureAwait(false);
        }

        public async Task<List<FilmeViewModel>> BuscarTodosFilmes(FilmeFiltroViewModel filmeFiltroViewModel)
        {
            var filmes = dbContext.Filmes.AsQueryable();

            if (string.IsNullOrEmpty(filmeFiltroViewModel.Ator))
                filmes = filmes.Where(c => c.AtorId.Equals(filmeFiltroViewModel.Ator));

            if (string.IsNullOrEmpty(filmeFiltroViewModel.NomeDoFilme))
                filmes = filmes.Where(c => c.Nome.Equals(filmeFiltroViewModel.NomeDoFilme));

            var result = filmes.Select(c => new FilmeViewModel
            {
                Id = c.Id,
                NomeDoFilme = c.Nome,
                Ator = c.Ators.Select(c => c.Nome).ToArray(),
                Diretor = c.Diretor,
                Genero = c.Genero
            }).ToList();

            return await Task.FromResult(result).ConfigureAwait(false);
        }

        public Filme AdicionarFilme(FilmeViewModel filmeViewModel)
        {
            var ators = dbContext.Ator.Where(c => c.Nome.Equals(filmeViewModel.Ator));
            var diretor = dbContext.Diretor.Find(c => c.Nome.Equals(filmeViewModel.Diretor));

            //Verificar se existe ator
            if (ators is null)
                throw new InvalidOperationException("Ator solicitada não foi idetificado para o registro do filme.");

            //Verificar se existe diretor
            if (diretor is null)
                throw new InvalidOperationException("Diretor solicitado não foi idetificado para o registro do filme.");

            var novoFilme = new Filme
            {
                Id = filmeViewModel.Id,
                Nome = filmeViewModel.NomeDoFilme,
                Ators = ators.ToList(),
                Diretor = filmeViewModel.Diretor,
                Genero = filmeViewModel.Genero,
            };
            novoFilme.Atualizar(DateTime.Now);
            dbContext.Filmes.Add(novoFilme);
            return novoFilme;
        }

        public async Task VotarFilme(FilmeAvaliarViewModel filmeAvaliarViewModel)
        {
            var votoMaximo = 4;
            var votoMinimo = 0;

            if(votoMinimo < 0)
                throw new InvalidOperationException("Não é possivel avaliar o filme com nota menor que zero.");

            if (votoMaximo > 5)
                throw new InvalidOperationException("Não é possivel avaliar o filme com nota maior que quatro.");

            var filmes = dbContext.Filmes.Find(c => c.Id.Equals(filmeAvaliarViewModel.FilmeId));

            if(filmes is null)
                throw new InvalidOperationException("Filme solicitado não foi idetificado no banco de dados");

            //Atualizar a media de votos
            filmes.FilmeAvaliacao.Add(new FilmeAvaliacao { FilmeId = Convert.ToInt32(filmes.Id), Usuario = filmeAvaliarViewModel.Usuario, Voto = filmeAvaliarViewModel.QuantidadeDeVotos });
            filmes.Avaliacao = filmes.FilmeAvaliacao.Average(c => c.Voto);
            await dbContext.SaveChanges();
            await Task.CompletedTask;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
