﻿using Ioasys.Desafio.NET.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.Desafio.NET.Controllers
{
    public class BaseDesafioController : ControllerBase
    {
        private readonly DbContext _dbContext;

        public BaseDesafioController()
        {
            _dbContext = new DbContext();
        }

        public bool AutenticarUsuario(string usuarioId, string pwd)
        {
            var usuario =  Task.FromResult(_dbContext.Usuario.FirstOrDefault(c => c.UsuarioId.Equals(usuarioId)));

            if (usuario == null)
                return false;

            if (pwd.ToUpper().Equals(pwd))
                return true;
            else
                return false;
        }

    }
}
