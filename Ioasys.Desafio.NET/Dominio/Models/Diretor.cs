﻿using System;

namespace Ioasys.Desafio.NET.Dominio.Models
{
    public class Diretor
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public int Idade { get; set; }
    }
}
