﻿using System;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Desafio.NET.Dominio.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Descriçao { get; set; }
        public int Idade { get; set; }
        public DateTime CriadoEm { get; set; }
    }
}
