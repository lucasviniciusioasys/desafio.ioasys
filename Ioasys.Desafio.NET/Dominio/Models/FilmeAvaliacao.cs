﻿namespace Ioasys.Desafio.NET.Dominio.Models
{
    public class FilmeAvaliacao
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public int FilmeId { get; set; }
        public int Voto { get; set; }
    }
}
