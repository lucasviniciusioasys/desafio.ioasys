﻿using Ioasys.Desafio.NET.Dominio.Models;
using Ioasys.Desafio.NET.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Desafio.NET.Dominio.Interfaces
{
    public interface IFilmeService : IDisposable
    {
        Task<List<Filme>> BuscarTodosFilmes(CancellationToken cancellationToken);
        Task<List<FilmeViewModel>> BuscarTodosFilmes(FilmeFiltroViewModel filmeFiltroViewModel);
        Task<Filme> BuscarByIdAsync(int filmeID);
        Filme AdicionarFilme(FilmeViewModel filmeViewModel);
        Task VotarFilme(FilmeAvaliarViewModel filmeAvaliarViewModel);
    }
}
